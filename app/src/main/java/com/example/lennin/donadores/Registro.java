package com.example.lennin.donadores;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by lennin on 06/12/2015.
 */
public class Registro extends android.support.v4.app.Fragment implements View.OnClickListener {
    public static final String Tag = Login.class.getSimpleName();
    private String myValue;
    private JSONObject jObject;
    private JSONArray categorias;
    private EditText donacion;
    //private ArrayList<String> catego = new ArrayList<String>();
    private List<StringWithTag> catego = new ArrayList<StringWithTag>();


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.registro_fragment, null);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myValue = this.getArguments().getString("message");
        try {
            jObject = new JSONObject(myValue);
            //Toast.makeText(getActivity(),String.valueOf(jObject),Toast.LENGTH_LONG).show();
            categorias = new JSONArray(jObject.getString("categorias"));

            //Toast.makeText(getActivity(), String.valueOf(categorias), Toast.LENGTH_LONG).show();


            //String surname = jso3.getString("Apellidos");
            //System.out.println(surname);

            //int date = jso3.getInt("Año_nacimiento");
            //System.out.println(date);

            //JSONArray jsa2 = jso3.getJSONArray ("Nombres_Hijos");
            //String names = jsa2.toString();
            for (int i = 0; i < categorias.length(); i++) {
                JSONObject jsonObject = categorias.getJSONObject(i);
                //System.out.println(jsonObject.opt("id_tipo_donador"));

                catego.add(new StringWithTag(jsonObject.optString("descripcion"), jsonObject.optString("id_tipo_donador")));
                //catego.add(jsonObject.optString("descripcion"));

            }
            //jso3.toString(myValue);
        } catch (Exception e) {

        }

        Spinner mySpinner = (Spinner) view.findViewById(R.id.spTipoDonador);
        donacion = (EditText) view.findViewById(R.id.etDonacion);
        Button botonRegistro = (Button) view.findViewById(R.id.btnRegistrarse);
        botonRegistro.setOnClickListener(this);


        mySpinner.setAdapter(new ArrayAdapter<StringWithTag>(getActivity(), android.R.layout.simple_spinner_dropdown_item, catego));
        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(pos);
                //System.out.println(s);
                Log.i("ese", String.valueOf(s));
                if (String.valueOf(s).contentEquals("sangre")) {
                    donacion.setHint("Tipo de sangre");
                } else if (String.valueOf(s).contentEquals("organo")) {
                    donacion.setHint("Organo a donar");
                } else {
                    donacion.setHint("Donacion");
                }


                Object tag = s.tag;
                Log.i("objecto", String.valueOf(tag));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });


    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnRegistrarse){
            Log.i("Registro","xd");
        }
    }
}