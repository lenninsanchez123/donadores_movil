package com.example.lennin.donadores;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

/**
 * Created by lennin on 06/12/2015.
 */
public class Login extends android.support.v4.app.Fragment {
    public static final String Tag=Login.class.getSimpleName();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //inflater.
        return inflater.inflate(R.layout.login_layout,null);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView registro=(TextView)view.findViewById(R.id.txtRegistro);
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sincronizacion accesso=new Sincronizacion();
                try{
                    String response=accesso.execute().get();
                    //Toast.makeText(getActivity(),"return"+response,Toast.LENGTH_LONG).show();
                    android.support.v4.app.FragmentTransaction fragmentTransaction=getFragmentManager().beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("message", response);
                    fragmentTransaction.addToBackStack(Registro.Tag);
                    Registro fragInfo = new Registro();
                    fragInfo.setArguments(bundle);


                    fragmentTransaction.replace(R.id.layout,fragInfo,fragInfo.Tag);
                    fragmentTransaction.commit();
                }catch(Exception e){

                }


                //Toast.makeText(getActivity(),"hola",Toast.LENGTH_LONG).show();
                 /*Fragment fragmentRegistro = new Registro();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.login, fragmentRegistro);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();*/
            }
        });
    }
}


