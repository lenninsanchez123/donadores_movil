package com.example.lennin.donadores;

/**
 * Created by ie-soluciones on 07/12/15.
 */
public class StringWithTag {
    public String string;
    public Object tag;

    public StringWithTag(String stringPart, Object tagPart) {
        string = stringPart;
        tag = tagPart;
    }

    @Override
    public String toString() {
        return string;
    }
}